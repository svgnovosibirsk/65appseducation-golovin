import Foundation

public protocol NetworkServiceProtocol {
    func getCharactersArray() -> [Character]
    func fetchData(successCompletion: @escaping () -> Void, failure: @escaping (Error) -> Void)
}
