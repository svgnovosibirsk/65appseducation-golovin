import Foundation
import UIKit
import CodableModels

final public class MarvelCharacterNetworkService {
    var parser: MarvelCharacterParserProtocol?

// MARK: - Constants
    private enum UrlConstants {
        static let baseURLString = "http://gateway.marvel.com/"
        static let characterURLString = "/v1/public/characters?"
        static let apiKey = "0fda4b2f7a54b045bf25668c6c74ddce"
        static let timeStamp = "1"
        static let hash = "a93042d9e4c70d6c20398fa60d86827e"
        static let charactersURL = "https://gateway.marvel.com/" +
        "v1/public/characters?ts=\(timeStamp)" +
        "&apikey=\(apiKey)&hash=\(hash)"
    }

// MARK: - Properties
    var charArray = [Character]()

    private let configuration: URLSessionConfiguration = .default
    private lazy var urlSession: URLSession = .init(configuration: configuration)

    public init() {
        parser = MarvelCharacterParser()
    }
}

// MARK: - NetworkServiceProtocol Methods
extension MarvelCharacterNetworkService {

    public func getCharactersArray() -> [Character] {
        return charArray
    }

    public func fetchData(successCompletion: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        guard let url = URL(string: UrlConstants.charactersURL) else { return }
        let request: URLRequest = .init(url: url )
        let dataTask = urlSession.dataTask(with: request) { [weak self] data, response, error in
            guard
                error == nil,
                (response as? HTTPURLResponse)?.statusCode == 200,
                let data = data
            else {
                return
            }
            self?.charArray = self?.parser?.parseHeros(with: data) {
                successCompletion()
            } ?? [Character]()
        }
        dataTask.resume()
    }
}
