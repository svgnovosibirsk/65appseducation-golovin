Pod::Spec.new do |spec|
  spec.name        = "Networking"
  spec.version     = "0.0.1"
  spec.summary     = "Networking"
  spec.description = <<-DESC
  Networking
                   DESC
  spec.homepage      = "https://65apps.com"
  spec.license       = "BSD"
  spec.author        = { "Golovin" => "svgnovosibirsk@gmail.com" }
  spec.platform      = :ios, "15.0"
  spec.swift_version = "5.0"
  spec.source        = { :path => "." }
  spec.source_files  = "Networking/**/*.{h,m,swift,xib,storyboard}"
  spec.dependency    "CodableModels"
end
              
