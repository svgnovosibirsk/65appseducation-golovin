import Foundation

public struct Character: Decodable {
    public let id: Int?
    public let name: String?
    public let description: String?
    public let thumbnail: Thumbnail?

    public var imageURL: URL? {
        guard
            let thumbnail = self.thumbnail,
            let path = thumbnail.path,
            let ext = thumbnail.pathExtension
        else {
            return nil
        }

        let fullPath = path + "." + ext

        return URL(string: fullPath)
    }
}

public struct Thumbnail: Decodable {
    public let path: String?
    public let pathExtension: String?
}
