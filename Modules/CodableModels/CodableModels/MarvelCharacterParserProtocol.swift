import Foundation

public protocol MarvelCharacterParserProtocol {
    func parseHeros(with data: Data, completion: @escaping () -> Void) -> [Character]?
}
