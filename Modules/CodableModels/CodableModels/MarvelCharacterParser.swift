import Foundation

final public class MarvelCharacterParser {
    public init() {}
}

// MARK: - MarvelCharacterParserProtocol
extension MarvelCharacterParser: MarvelCharacterParserProtocol {
    public func parseHeros(with data: Data, completion: @escaping () -> Void) -> [Character]? {
        var charArray = [Character]()
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data)
            guard
                let json = jsonObject as? [String: Any],
                let data = json["data"] as? [String: Any],
                let results = data["results"] as? [[String: Any]]
            else {
                return nil
            }
            for index in 0..<results.count {
                let id = results[index]["id"] as? Int
                let name = results[index]["name"] as? String
                let description = results[index]["description"] as? String
                let thumbnail = results[index]["thumbnail"] as? [String: Any]
                guard let imagePath = thumbnail?["path"] as? String,
                      let ext = thumbnail?["extension"] as? String
                else {
                          return nil
                      }
                let thumb = Thumbnail(path: imagePath, pathExtension: ext)
                let character = Character(id: id, name: name, description: description, thumbnail: thumb)
                charArray.append(character)
            }
            completion()
        } catch {
           print(error)
        }
        return charArray
    }
}
