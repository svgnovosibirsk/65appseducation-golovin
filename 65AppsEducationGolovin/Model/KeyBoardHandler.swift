//
//  KeyBoardHandler.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 17.10.2021.
//

import UIKit

class KeyBoardHandler {

    static let shared = KeyBoardHandler()

    func startKeyBoardObservation() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification, for view: UIView) {
        guard let keyboardSize =
                (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else { return }
      view.frame.origin.y = 0 - keyboardSize.height
    }

    @objc func keyboardWillHide(notification: NSNotification, for view: UIView) {

      view.frame.origin.y = 0
    }
}
