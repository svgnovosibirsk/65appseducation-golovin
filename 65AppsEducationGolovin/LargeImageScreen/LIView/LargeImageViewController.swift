//
//  LargeImageViewController.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 10.01.2022.
//

import UIKit
import CodableModels

final class LargeImageViewController: UIViewController {
    var character: Character?

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTrailingConstraint: NSLayoutConstraint!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        setupView()
    }

    func updateMinZoomScaleForSize(_ size: CGSize) {
      let widthScale = size.width / (imageView.bounds.width * 3)
      let heightScale = size.height / (imageView.bounds.height * 3)
      let minScale = min(widthScale, heightScale)

      scrollView.minimumZoomScale = minScale
      scrollView.zoomScale = minScale
    }

    override func viewWillLayoutSubviews() {
      super.viewWillLayoutSubviews()
      updateMinZoomScaleForSize(view.bounds.size)
    }
}

private extension LargeImageViewController {
    func setupView() {
        DispatchQueue.main.async {[weak self] in
            self?.imageView?.kf.setImage(with: self?.character?.imageURL)
        }
    }
}

extension LargeImageViewController: LargeImageViewControllerProtocol {

}

extension LargeImageViewController: UIScrollViewDelegate {
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return imageView
  }
}
