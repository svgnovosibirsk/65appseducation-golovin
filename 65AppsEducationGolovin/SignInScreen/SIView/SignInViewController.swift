//
//  SignInViewController.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 09.10.2021.
//
//  При разработке и тестировании приложения в Firebase были созданы аккаутны:
//  user: a@a.com password:123456
//  user: b@b.com password:123456
//  user: c@c.com password:123456
//  user: d@d.com password:123456

import UIKit
import FirebaseAuth

final class SignInViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var appLabel: UILabel!
    @IBOutlet weak var emailTextfield: InsetTextField!
    @IBOutlet weak var passwordTextfield: InsetTextField!

    // MARK: - Properties
    private var presenter: SignInPresenterProtocol?

    // MARK: - Constants
    private enum Constants {
        static let keyboardOffset: CGFloat = 100
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SignInPresenter(view: self)
        setDismissKeyboardGesture()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray]
        startKeyBoardObservation()
        setAppLableFont()
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = 0 - Constants.keyboardOffset
    }

    @objc func keyboardWillHide(notification: NSNotification) {
      self.view.frame.origin.y = 0
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func signInBtnPressed(_ sender: UIButton) {
        presenter?.signInWith(email: emailTextfield.text!, andPassword: passwordTextfield.text!)
    }

    func moveToCharacterVC() {
        self.performSegue(withIdentifier: "toCharFromSign", sender: self)
    }

    // MARK: - Set Font
    private func setAppLableFont() {
        guard let customFont = UIFont(name: "OpenSansRoman-ExtraBold", size: 48) else {
            fatalError("""
                Failed to load the "OpenSansRoman-ExtraBold" font.
                """
            )
        }
        appLabel.font = UIFontMetrics.default.scaledFont(for: customFont)
        appLabel.adjustsFontForContentSizeCategory = true
    }

    // MARK: - Dismiss keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    private func setDismissKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

}

    // MARK: - KeyBoard handling
 extension SignInViewController {
    func startKeyBoardObservation() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(SignInViewController.keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(SignInViewController.keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
 }

// MARK: - SignInViewProtocol Methods
extension SignInViewController: SignInViewProtocol {
}
