//
//  SignInPresenter.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 18.11.2021.
//

import Foundation

final class SignInPresenter {
    // MARK: - Properties
    weak var view: SignInViewProtocol?
    var firebaseManager: FirebaseManagerProtocol?

    // MARK: - Initializers
    init(view: SignInViewProtocol) {
        self.view = view
        firebaseManager = FirebaseManager()
    }
}

// MARK: - SignInPresenterProtocol Methods
extension SignInPresenter: SignInPresenterProtocol {
    func signInWith(email: String, andPassword password: String) {
        firebaseManager?.signInWith(email: email, andPassword: password) {
            self.view?.moveToCharacterVC()
        }
    }
}
