//
//  FavouritesPresenter.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 15.01.2022.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import CodableModels

final class FavouritesPresenter {
    // MARK: - Properties
    var favouriteView: FavouritesViewControllerProtocol?

    // MARK: - Initializers
    init(view: FavouritesViewControllerProtocol) {
        favouriteView = view
    }
}

    // MARK: - FavouritesPresenterProtocol
extension FavouritesPresenter: FavouritesPresenterProtocol {
    func fetchCharactersFromDB() {
        let favouriteCharactersDB = Database.database().reference().child("Favourites")
        let favouriteCharactersDBWithUser = favouriteCharactersDB.child(Auth.auth().currentUser?.uid ?? "uid")

        favouriteCharactersDBWithUser.observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value as? [String: Any] else { return }
            for item in value.keys {
                if let hero = value[item] as? [String: String] {
                    let character = FavouriteCharacter(name: hero["Character"],
                                                       description: hero["Description"],
                                                       imagePath: hero["ImagePath"],
                                                       imageExtention: hero["ImageExtention"])
                    self.favouriteView?.addCharacter(character: character)
                }
            }
            self.favouriteView?.update()
        }
    }

}
