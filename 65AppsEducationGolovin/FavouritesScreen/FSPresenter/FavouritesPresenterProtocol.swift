//
//  FavouritesPresenterProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 15.01.2022.
//

import Foundation

protocol FavouritesPresenterProtocol {
    func fetchCharactersFromDB()
}
