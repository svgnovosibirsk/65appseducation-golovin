//
//  FavouritesViewController.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 15.01.2022.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import CodableModels
import SwiftUI

struct FavouriteCharacter {
    let name: String?
    let description: String?
    let imagePath: String?
    let imageExtention: String?

    var imageURL: URL? {
        guard
            let path = imagePath,
            let ext = imageExtention
        else {
            return nil
        }
        let fullPath = path + "." + ext
        return URL(string: fullPath)
    }
}

final class FavouritesViewController: UIViewController {
    // MARK: - Constants
    private enum Constants {
        static let rowHeight: CGFloat = 90
        static let textLabelFontSize: CGFloat = 20
        static let detailTextLabelFontSize: CGFloat = 18
        static let imageViewCcornerRadius: CGFloat = 40
        static let cellID = "favourCell"
    }

    // MARK: - Properties
    var characters = [FavouriteCharacter]()
    var presenter: FavouritesPresenterProtocol?

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = FavouritesPresenter(view: self)
        tableView.dataSource = self
        tableView.rowHeight = 100
        fetchFavouriteCharactersFromDB()
    }

    func fetchFavouriteCharactersFromDB() {
        presenter?.fetchCharactersFromDB()
    }
}

    // MARK: - UITableViewDataSource
extension FavouritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID, for: indexPath)
        cell.textLabel?.text = characters[indexPath.row].name
        cell.detailTextLabel?.text = characters[indexPath.row].description

        DispatchQueue.main.async {
            cell.imageView?.kf.setImage(with: self.characters[indexPath.row].imageURL)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
}

extension FavouritesViewController: FavouritesViewControllerProtocol {
    func addCharacter(character: FavouriteCharacter) {
        self.characters.append(character)
    }

    func update() {
        self.tableView.reloadData()
    }
}
