//
//  FavouritesViewControllerProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 15.01.2022.
//

import Foundation
import CodableModels

protocol FavouritesViewControllerProtocol {
    func addCharacter(character: FavouriteCharacter)
    func update()
}
