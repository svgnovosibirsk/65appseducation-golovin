//
//  CreateAccountViewProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 18.11.2021.
//

import Foundation

protocol CreateAccountViewProtocol: AnyObject {
    func moveToCharacterVC()
}
