//
//  CreateAccountViewController.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 17.10.2021.
//
//  При разработке и тестировании приложения в Firebase были созданы аккаутны:
//  user: a@a.com password:123456
//  user: b@b.com password:123456
//  user: c@c.com password:123456
//  user: d@d.com password:123456

import UIKit
import FirebaseAuth

final class CreateAccountViewController: UIViewController {
    // MARK: - Properties
    private var presenter: CreateAccountPresenterProtocol?

    // MARK: - Constants
    private enum Constants {
        static let keyboardOffset: CGFloat = 100
    }

    // MARK: - Outlets
    @IBOutlet weak var emailTextField: InsetTextField!
    @IBOutlet weak var passwordTextField: InsetTextField!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CreateAccountPresenter(view: self)
        navigationController?.navigationBar.topItem?.title = ""
        setDismissKeyboardGesture()
       // startKeyBoardObservation()
    }

    @objc func keyboardWillShow(notification: NSNotification, for view: UIView) {
        self.view.frame.origin.y = 0 - Constants.keyboardOffset
    }

    @objc func keyboardWillHide(notification: NSNotification, for view: UIView) {
        self.view.frame.origin.y = 0
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func createAccountPressed(_ sender: UIButton) {
        presenter?.createAccountWith(email: emailTextField.text, and: passwordTextField.text)
    }

    func moveToCharacterVC() {
        self.performSegue(withIdentifier: "toCharFromCreate", sender: self)
    }

    // MARK: - Dismiss keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    private func setDismissKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
}

    // MARK: - KeyBoard handling
 extension CreateAccountViewController {
    func startKeyBoardObservation() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CreateAccountViewController.keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CreateAccountViewController.keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
 }

// MARK: - CreateAccountViewProtocol
extension CreateAccountViewController: CreateAccountViewProtocol {

}
