//
//  CreateAccountPresenter.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 18.11.2021.
//

import Foundation

final class CreateAccountPresenter {
    // MARK: - Properties
    weak var view: CreateAccountViewProtocol?
    var firebaseManager: FirebaseManagerProtocol?

    // MARK: - Initializers
    init(view: CreateAccountViewProtocol) {
        self.view = view
        firebaseManager = FirebaseManager()
    }
}

// MARK: - CreateAccountPresenterProtocol Methods
extension CreateAccountPresenter: CreateAccountPresenterProtocol {
    func createAccountWith(email: String?, and password: String?) {
        guard email?.count ?? 0 > 0, password?.count ?? 0 > 0 else { return }

        firebaseManager?.createAccountWith(email: email, and: password) {
            self.view?.moveToCharacterVC()
        }
    }
}
