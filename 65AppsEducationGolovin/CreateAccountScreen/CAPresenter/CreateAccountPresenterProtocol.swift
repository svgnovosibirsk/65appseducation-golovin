//
//  CreateAccountPresenterProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 18.11.2021.
//

import Foundation

protocol CreateAccountPresenterProtocol {
    func createAccountWith(email: String?, and password: String?)
}
