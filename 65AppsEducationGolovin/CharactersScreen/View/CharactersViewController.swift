//
//  CharactersViewController.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 29.11.2021.
//

import UIKit
import Kingfisher
import CodableModels
import Networking
import FirebaseAuth

final class CharactersViewController: UIViewController {
    // MARK: - Constants
    private enum Constants {
        static let rowHeight: CGFloat = 90
        static let textLabelFontSize: CGFloat = 20
        static let detailTextLabelFontSize: CGFloat = 18
        static let imageViewCcornerRadius: CGFloat = 40
        static let cellID = "CharacterCell"
    }

    // MARK: - Properties
    var presenter: CharactersPresenter?

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.rowHeight = 100
        presenter = CharactersPresenter(view: self)
        self.navigationController?.navigationBar.tintColor = .red
    }

    @IBAction func logOutBtnPressed(_ sender: UIBarButtonItem) {
        presenter?.logOut()

        guard (navigationController?.popToRootViewController(animated: true)) != nil else {
            print("No View Controllers to pop off")
            return
        }
    }

    @IBAction func favouritesBtnPressed(_ sender: UIBarButtonItem) {
        print("favouritesBtnPressed")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = tableView.indexPathForSelectedRow
        let charactersArray = presenter?.getCharactersArray()
        let character = charactersArray?[indexPath?.row ?? 0]

        let viewController = segue.destination as? DetailViewController
        viewController?.character = character
    }

}

// MARK: - CharactersViewControllerProrocol
extension CharactersViewController: CharactersViewControllerProrocol {
    func updateTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource
extension CharactersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.getCharactersArray().count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID, for: indexPath)
        if presenter?.getCharactersArray() != nil {
          configureCell(cell, with: indexPath)
        } else {
            cell.textLabel?.text = "Name"
            cell.detailTextLabel?.text = "Description"
            cell.imageView?.image = UIImage.init(systemName: "person")
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
}

// MARK: - Private Methods
private extension CharactersViewController {
    private func configureCell(_ cell: UITableViewCell, with indexPath: IndexPath) {
        cell.textLabel?.text = presenter?.getCharactersArray()[indexPath.row].name
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: Constants.textLabelFontSize)
        cell.detailTextLabel?.text = presenter?.getCharactersArray()[indexPath.row].description
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: Constants.detailTextLabelFontSize)
        cell.imageView?.layer.cornerRadius = Constants.imageViewCcornerRadius
        cell.imageView?.clipsToBounds = true

        DispatchQueue.main.async {
            let character = self.presenter?.getCharactersArray()[indexPath.row]
            cell.imageView?.kf.setImage(with: character?.imageURL)
        }
    }
}
