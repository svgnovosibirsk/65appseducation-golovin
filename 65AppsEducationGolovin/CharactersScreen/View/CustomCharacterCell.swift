//
//  CustomCharacterCell.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 02.12.2021.
//

import UIKit

class CustomCharacterCell: UITableViewCell {
    // MARK: - Constants
    private enum Constants {
        static let imageViewRectStartX: CGFloat = 0
        static let imageViewRectStartY: CGFloat = 0
        static let imageViewWidth: CGFloat = 80
        static let imageViewHeight: CGFloat = 80
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView?.frame = CGRect(x: Constants.imageViewRectStartX,
                                       y: Constants.imageViewRectStartY,
                                       width: Constants.imageViewWidth,
                                       height: Constants.imageViewHeight)
        self.imageView?.contentMode = UIView.ContentMode.scaleToFill
    }
}
