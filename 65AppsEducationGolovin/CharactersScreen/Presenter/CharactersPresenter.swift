//
//  CharactersPresenter.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 29.11.2021.
//

import Foundation
import CodableModels
import Networking

final class CharactersPresenter {
    // MARK: - Properties
    var characterModel: MarvelCharacterNetworkService
    var characterView: CharactersViewControllerProrocol?
    var firebaseManager: FirebaseManagerProtocol?

    // MARK: - Initializers
    init(view: CharactersViewControllerProrocol ) {
        characterModel = MarvelCharacterNetworkService()
        characterView = view
        updateModel {
            self.characterView?.updateTableView()
        }
        firebaseManager = FirebaseManager()
    }
}

// MARK: - CharactersPresenterProtocol
extension CharactersPresenter {
    func getCharactersArray() -> [Character] {
        self.characterModel.getCharactersArray()
    }

    func logOut() {
        firebaseManager?.logOut()
    }
}

private extension CharactersPresenter {
    private func updateModel(completion: @escaping () -> Void) {
        characterModel.fetchData(successCompletion: {
            completion()
        }, failure: { error in
            print(error)
        })
    }
}
