//
//  CharactersPresenterProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 29.11.2021.
//

import Foundation

protocol CharactersPresenterProtocol {
    func getCharactersArray() -> [Character]
}
