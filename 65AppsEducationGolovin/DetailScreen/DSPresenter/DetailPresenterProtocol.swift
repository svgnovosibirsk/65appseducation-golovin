//
//  DetailPresenterProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 09.01.2022.
//

import Foundation
import CodableModels

protocol DetailPresenterProtocol {
    func addToFavourites(character: Character? )
}
