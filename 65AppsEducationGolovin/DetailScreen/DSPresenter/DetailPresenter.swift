//
//  DetailPresenter.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 09.01.2022.
//

import Foundation
import CodableModels
import FirebaseDatabase
import FirebaseAuth

class DetailPresenter {
    // MARK: - Properties
    var detailView: DetailViewControllerProtocol?
    var firebaseManager: FirebaseManagerProtocol?

    // MARK: - Initializers
    init(view: DetailViewControllerProtocol ) {
        detailView = view
        firebaseManager = FirebaseManager()
    }
}

    // MARK: - DetailPresenterProtocol
extension DetailPresenter: DetailPresenterProtocol {
    func addToFavourites(character: Character? ) {

        firebaseManager?.canBeAdded(name: character?.name ?? "0") { canAdd in
            if canAdd == true {
                self.addFavourites(character: character)
            } else {
                return
            }
        }
    }

    func addFavourites(character: Character?) {
        detailView?.favouritesBtn.isEnabled = false

        let favouriteCharactersDB = Database.database().reference().child("Favourites")
        let favouriteCharactersDBWithUser = favouriteCharactersDB.child(Auth.auth().currentUser?.uid ?? "uid")
        let characterDictionary = ["User": Auth.auth().currentUser?.email ?? "",
                                   "Character": character?.name ?? "",
                                   "Description": character?.description ?? "",
                                   "ImagePath": character?.thumbnail?.path ?? "",
                                   "ImageExtention": character?.thumbnail?.pathExtension ?? ""]

        favouriteCharactersDBWithUser.childByAutoId().setValue(characterDictionary) { error, _ in
            if error != nil {
                print("Data Base saving error: \(error!)")
            } else {
                self.detailView?.favouritesBtn.isEnabled = true
            }
        }
    }
}
