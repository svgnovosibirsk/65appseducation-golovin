//
//  DetailViewController.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 09.01.2022.
//

import UIKit
import CodableModels
import Kingfisher
import FirebaseDatabase
import FirebaseAuth

final class DetailViewController: UIViewController {
    // MARK: - Properties
    var character: Character?
    var presenter: DetailPresenterProtocol?

    // MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var favouritesBtn: UIBarButtonItem!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = DetailPresenter(view: self)
        setupView()
        setImageGestureRecognizer()
    }

    // MARK: - IBActions
    @IBAction func addToFavouritesPressed(_ sender: UIBarButtonItem) {
        presenter?.addToFavourites(character: character)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as? LargeImageViewController
        viewController?.character = character
    }
}

    // MARK: - Private methods
private extension DetailViewController {
    func setupView() {
        nameLbl.text = character?.name
        descriptionTextView.text = character?.description

        DispatchQueue.main.async {[weak self] in
            self?.imageView?.kf.setImage(with: self?.character?.imageURL)
        }
    }

    func setImageGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imageView.addGestureRecognizer(tap)
        imageView.isUserInteractionEnabled = true
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "toLargeImageVC", sender: self)
    }
}

extension DetailViewController: DetailViewControllerProtocol {

}
