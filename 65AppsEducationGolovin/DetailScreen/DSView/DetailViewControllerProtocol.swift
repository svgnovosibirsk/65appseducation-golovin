//
//  DetailViewControllerProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 09.01.2022.
//

import UIKit

protocol DetailViewControllerProtocol {
    var favouritesBtn: UIBarButtonItem! { get }
}
