//
//  FirebaseManagerProtocol.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 29.01.2022.
//

import Foundation

protocol FirebaseManagerProtocol {
    func logOut()
    func createAccountWith(email: String?, and password: String?, complition: @escaping () -> Void)
    func signInWith(email: String, andPassword password: String, complition: @escaping () -> Void)
    func canBeAdded(name: String, completion: @escaping(Bool) -> Void)
}
