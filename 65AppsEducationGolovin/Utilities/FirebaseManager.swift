//
//  FirebaseManager.swift
//  65AppsEducationGolovin
//
//  Created by Sergey Golovin on 29.01.2022.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import CodableModels

final class FirebaseManager {

}

extension FirebaseManager: FirebaseManagerProtocol {
    func logOut() {
        do {
            try Auth.auth().signOut()
        } catch {
            print("Sign Out error")
        }
    }

    func createAccountWith(email: String?, and password: String?, complition: @escaping () -> Void) {
        guard let email = email, let password = password else { return }
        Auth.auth().createUser(withEmail: email,
                               password: password) { (_, error) in
            if error != nil {
                print(error!)
            } else {
                complition()
            }
        }
    }

    func signInWith(email: String, andPassword password: String, complition: @escaping () -> Void) {
        Auth.auth().signIn(withEmail: email,
                           password: password) {(_, error) in
            if error != nil {
                print(error!)
            } else {
                complition()
            }
        }
    }

    func canBeAdded(name: String, completion: @escaping(Bool) -> Void) {
        let favouriteCharactersDB = Database.database().reference().child("Favourites")
        let favouriteCharactersDBWithUser = favouriteCharactersDB.child(Auth.auth().currentUser?.uid ?? "uid")
        favouriteCharactersDBWithUser.observeSingleEvent(of: .value) { snapshot in
            var canAdd = false
            guard let value = snapshot.value as? [String: Any] else { return }
            for item in value.keys {
                if let hero = value[item] as? [String: String] {
                    if name == hero["Character"] {
                        canAdd = false
                    } else {
                        canAdd = true
                    }
                }
            }
            completion(canAdd)
        }
    }
}
